# MCproduction
This repository is for producing truth level MC DAOD samples using madgraph.

First think of a name for the sample, say it is [NAME]
Make sure the JO is store in the folder:JO_[NAME].
Make sure that there is only one JO with proper names.

Do:
```
. ./run.sh [NAME] [MaxEvnt]
```

If [MaxEvnt] is not given, it would be set to 100.

The output would be in EOS with a folder named:[NAME]
The DAOD could then be analyzed by athena.
