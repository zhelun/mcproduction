import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*2.2 if runArgs.maxEvents>0 else 2.2*evgenConfig.nEventsPerJob
#nevents=10000*1.1

process = """
import model loop_sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l = e- mu- ta- e+ mu+ ta+
define nu = ve vm vt ve~ vm~ vt~
define w = w+ w-
generate p p > w a a, (w > l nu)
output -f"""
process_dir = new_process(process)

#fcard = open('proc_card_mg5.dat','w')
#fcard.write("""
#    import model loop_sm
#    define p = g u c d s u~ c~ d~ s~
#    define l = e- mu- ta- e+ mu+ ta+
#    define nu = ve vm vt ve~ vm~ vt~
#    define w = w+ w-
#    generate p p > w a a, (w > l nu)
#    output -f
#    """)
#fcard.close()
#process_dir = new_process()

#Fetch default NLO run_card.dat and set parameters
settings = {'nevents':nevents}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#### Shower
evgenConfig.description = 'aMcAtNlo_Wlvaa'
evgenConfig.keywords+=['wlv','di-photon']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
