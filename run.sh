#!/bin/bash

setupATLAS
asetup 21.6.23,AthGeneration

#input
SampleName=$1
MaxEvent=$2
if [ -z "$2" ]  #-z checks if it is a null argument
  then
	MaxEvent=100 #so that it ends quickyly
fi
echo EventMax is set to $MaxEvent

JOname=JO_$SampleName
eos="/eos/user/z/zhelun"
current_folder=$PWD

cd $eos
rm -rf $SampleName
mkdir $SampleName
cd $SampleName
output_dir=$PWD
cd $current_folder
echo eos setup completed. Dir : $output_dir

rm -rf $SampleName
mkdir $SampleName
cd $SampleName
echo afs setup completed. Dir : $PWD

outputName=$output_dir/$SampleName.root
echo Output root file:$outputName

Gen_tf.py --ecmEnergy=13000. --maxEvents=$MaxEvent --firstEvent=1 --randomSeed=123456 --outputEVNTFile=$outputName --jobConfig=/afs/cern.ch/user/z/zhelun/athenaMC/$JOname

cd ..
asetup 21.2 ,AthDerivation,latest
cd $SampleName
Reco_tf.py --inputEVNTFile $outputName --outputDAODFile $SampleName.pool.root --reductionConf TRUTH1

mv DAOD_TRUTH1.$SampleName.pool.root $output_dir
